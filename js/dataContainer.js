/**
 * Created by jiayi.hu on 3/17/17.
 */
var d = new Date();
var day = d.getDate();
var month = d.getMonth() + 1;
var year = d.getFullYear();
new DateSelector({
    input: 'birChange',//点击触发插件的input框的id
    container: 'dataContainer',//插件插入的容器id
    type: 0,
    param: [1, 1, 1, 0, 0],
    beginTime: [2000, 1, 1],
    endTime: [2050, 12, 12],
    recentTime: [year, month, day],
    success: function (arr) {
        console.log(arr);
        controller.handleBirChange(arr);
    }//回调
});
new MultiPicker({
    input: 'cityChange',//点击触发插件的input框的id
    container: 'cityContainer',//插件插入的容器id
    jsonData:$city,
    success: function (arr) {
        if(!(arr.length == 2)){
            controller.handleCityChange(arr[0].value);
        }else {
            controller.handleCityChange(arr[1].value);
        }

    }//回调
});