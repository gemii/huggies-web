/**
 * Created by jiayi.hu on 3/22/17.
 */
var hosArr = new Array();
var infoData = new Object();
var dataModule = {
    url: {
        hosByCity: PREFIX + "HelperManage/enter/getHosByCity",
        userInfo: PREFIX + "GroupManage/haoqi/getUserInfoByOpenId",
        member: PREFIX + "HelperManage/enter/huggies/member",
        savepoint: PREFIX + "HelperManage/savepoint"
    },
    postSave: function (Data) {
        var url = this.url.savepoint;
        $.ajax({
            url:url,
            data:Data,
            type:"POST",
            success: function (res) {
                //success
            },
            error: function () {
                //error
            }
        })
    },
    postMemberInfo: function (Data,e) {
        var url = this.url.member;
        $.ajax({
            type: "POST",
            data: Data,
            url: url,
            dataType: "json",
            cache: false,
            beforeSend: function () {
                $(e).val("提交中");
                $(e).attr("disabled",true);
            },
            success: function (json) {
                switch (eval(json).status){
                    case 200:
                        var monitorSuc = new Object();
                        monitorSuc.content = infoData.openid+":"+infoData.nickname+":"+Data.phone;
                        monitorSuc.pointName = "用户匹配";
                        monitorSuc.pageName = "展示二维码页面";
                        monitorSuc.userKey = infoData.openid;
                        dataModule.postSave(monitorSuc);
                        window.location.href = "QRcode.html?QRcodeUrl="+eval(json).data.robotID;
                        break;
                    case -1:
                        var monitorFail = new Object();
                        monitorFail.content = infoData.openid+":"+infoData.nickname+":"+Data.phone;
                        monitorFail.pointName = "用户未匹配";
                        monitorFail.pageName = "不展示二维码页面";
                        monitorFail.userKey = infoData.openid;
                        dataModule.postSave(monitorFail);
                        window.location.href = "sorry.html";
                        break;
                    case -2:
                        alert("达到入群人数限制")
                        break;
                    case -3:
                        alert("重复申请")
                        break;
                    case -4:
                        alert("没有找到合适的群")
                        break;
                    case 500:
                        alert("提交出错,请重试!")
                }
            },
            error: function () {
                alert("提交出错,请重试!")
            },
            complete:function () {
                $(e).val("提交");
                $(e).removeAttr("disabled");
            }

        })
    },
    getUserInfo: function (openid) {
        var url = this.url.userInfo + '?openId=' + openid;
        $.ajax({
            type: "GET",
            url: url,
            dataType: "json",
            cache: false,
            success: function (res) {
                infoData = res.resultContent.userInfo;
                if(res.resultCode == 100){
                    var monitor = {};
                    monitor.content = infoData.openid+":"+infoData.nickname;
                    monitor.pointName = "点击消息";
                    monitor.pageName = "用户信息填写页面";
                    monitor.userKey = infoData.openid;
                    dataModule.postSave(monitor);
                }else{
                    alert('请求失败');
                }
            },
            error: function () {
                alert('请求失败');
            }
        })
    },
    getHosByCity: function (Data) {
        var url = this.url.hosByCity + "?city=" + Data;
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            cache: false,
            success: function (json) {
                hosArr.splice(0, hosArr.length);
                if (!(eval(json).data.length == 0)) {
                    for (var id in eval(json).data) {
                        hosArr.push({"value": eval(json).data[id]})
                    }
                    hosArr.push({"value":"其他"})
                } else {
                    hosArr.push({"value": "其他"})
                }
                new MultiPicker({
                    input: 'hospitalChange',//点击触发插件的input框的id
                    container: 'hospitalContainer',//插件插入的容器id
                    jsonData: hosArr,
                    success: function (arr) {
                        controller.handleHospitalChange(arr);
                    }//回调
                });
            },
            error: function () {
            }
        })
    }
}