/**
 * Created by jiayi.hu on 3/16/17.
 */

var controller = {
    init: function () {
        document.getElementById('submit').onclick = this.submit;
        eventFunctions.pullCookie();
    },
    handleCityChange: function (value) {
        $("input[id = " + "cityChange" + "]").val(value);
        $("input[id = " + "hospitalChange" + "]").val("");
        //请求医院名称
        dataModule.getHosByCity(value);
    },
    handleHospitalChange: function (arr) {
        if (arr.length == 1) {
            $("input[id = " + "hospitalChange" + "]").val(arr[0].value);
        }
    },
    handleBirChange: function (arr) {
        if (arr.length == 3) {
            $("input[id = " + "birChange" + "]").val(arr[0] + "-" + arr[1] + "-" + arr[2]);
        }
    },
    submit: function () {
        var monitor = new Object();
        monitor.content = infoData.openid+":"+infoData.nickname;
        monitor.pointName = "H5提交";
        monitor.pageName = "用户信息填写页面";
        monitor.userKey = infoData.openid;
        dataModule.postSave(monitor);
        eventFunctions.pushCookie(this);
    }
}
var eventFunctions = {
    pullCookie: function () {
        var current;
        var self = this;
        if (!($.cookie("data") == undefined)) {
            current = JSON.parse($.cookie("data"));
            if (current == undefined || current == "" || current == null) {
                return;
            }
            console.log(current);
            //获取cookie中的值
            for (var k in current) {
                console.log(k, current[k])
                $("input[name=" + k + "]").val(current[k])
            }
        }
        dataModule.getUserInfo(getQueryString('openid'))
    },
    pushCookie: function (e) {
        var judgeFlag = true;
        var inputData = new Object();
        var cookieData = new Object();
        var allInput = document.querySelectorAll("input[type=text]");
        $.each(allInput, function (k, v) {
            if(v.value == ""){
                judgeFlag = false;
                return false;
            }
            inputData[v.name] = v.value
            if(v.name == "edc"){
                cookieData[v.name] = v.value
            }
        })
        //手机号验证和非空验证
        try {
            if(judgeFlag == false){
                throw "完善信息"
            }
        }catch (err){
            alert(err);
            return false;
        }
        try {
            var phoneInput = $("input[name=phone]");
            if (!(/^1[34578]\d{9}$/.test(phoneInput.val()))) {
                phoneInput.focus();
                throw "手机号错误";
            }
        }catch (err){
            alert(err);
            return false;
        }
        //存入cookie
        $.cookie("data", JSON.stringify(cookieData), {"expires": 1});
        //提交给后端
        var sendData = new Object();
        for (var i in inputData) {
            sendData[i] = inputData[i];
        }
        for (var j in infoData) {
            if(j=="openid"){
                sendData["openid"] = infoData[j];
            }
            if(j=="nickname"||j=="headimgurl"){
                sendData[j] = infoData[j];
            }
        }
        dataModule.postMemberInfo(sendData,e)
    }
}